<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta -->
    <title>
        Yuvraj Jhala
    </title>
    <meta charset="UTF-8">
    <meta name="author" content="Yuvraj Jhala">
    <meta name="keywords" content="PHP, Yii, Yii2, WordPress, OpenCart, Web Services, Web Development, HTML, CSS, jQuery, JavaScript">
    <meta name="description" content="A web developer, designer and a full time freelancer living in Ahemdabad, IN. Focusing mainly on WordPress, Responsive Design, Restful web services and MVC Frameworks (Yii, Symfony and Code Ignitor)">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Stylesheets -->
    <link href='https://fonts.googleapis.com/css?family=Inconsolata' rel='stylesheet' type='text/css'>
    <link href="plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/main.css?v=1.0.1">
    <link href="plugins/changethewords/animate.changethewords.css" rel="stylesheet" type="text/css">
    <link href="plugins/components-font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel='shortcut icon' href='images/favicon.ico' type='image/x-icon'/ >
</head>
<body>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-47388802-1', 'auto');
    ga('send', 'pageview');
</script>
<div class="wrapper">
    <div class="hero">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!--<div class="navbar-header">
                    <a class="navbar-brand" href="#">WebSiteName</a>
                </div>-->
                <ul class="nav navbar-nav pull-right">
                    <li><a href="mailto:png625@gmail.com"><i class="fa fa-envelope-square fa-2x"></i></a></li>
                    <li><a target="_blank" href="http://twitter.com/imyuvii"><i class="fa fa-twitter-square fa-2x"></i></a></li>
                    <li><a target="_blank" href="https://gitlab.com/u/imyuvii"><i class="fa fa-git-square fa-2x"></i></a></li>
                    <li><a target="_blank" href="http://in.linkedin.com/in/imyuvii"><i class="fa fa-linkedin-square fa-2x"></i></a></li>
                </ul>
            </div>
        </nav>
        <div class="row no-padding">
            <div class="display-picture col-md-12">
                <img id="yuvraj" src="images/dp.jpg" alt="DP"/>
                <!-- <span id="yuvraj"></span>-->
            </div>
        </div>
        <div class="heading row">
            <div class="container">
                <div class="col-md-12 text-center">
                    <a href="http://imyuvii.com" class="logo">Yuvraj Jhala</a>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row main-con">
                <!--<div class="label">
                    <span id="yuvraj"></span>
                </div>-->
                <div class="text-center content-heading">
                    <p>Hi! I'm Yuvraj Jhala, a web developer, designer and full time freelancer living in Ahmedabad, IN. Focusing mainly on WordPress, Responsive Design, Restful web services and MVC Frameworks (Yii, Symfony and Code Ignitor). I am also a web development consultant for the brilliant team at <a target="_blank" href="https://www.linkedin.com/company/xovak-studio"><strong>Xovac Studio Arts</strong></a>.</p>
                </div>
                <div id="w" class="skills-effect text-center">
                    <span data-id="1">PHP</span>
                    <span data-id="4">Node JS</span>
                    <span data-id="2">Electron</span>
                    <span data-id="3">Yii</span>
                    <span data-id="11">CSS</span>
                    <span data-id="5">WordPress</span>
                    <span data-id="6">JavaScript</span>
                    <span data-id="7">OpenCart</span>
                    <span data-id="8">jQuery</span>
                    <span data-id="9">Laravel</span>
                    <span data-id="10">MySQL</span>
                    <span data-id="12">Embedded JS</span>
                    <span data-id="3">HTML</span>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <!-- Background -->
        <div class="row">
            <div class="label">
                <span class="num">One.</span>
                <span class="name">Background</span>
            </div>
            <div class="content">
                <p>My interest in web development stemmed from my passion for gaming. Between developing websites and playing video games,
                    I admittedly spend most of my time in front of a screen.
                    Some of my passions are writing clean code, creating best user experiences and filling up a fresh cup of tea.</p>
                <p>I live in <span class="big">India</span>. I love to <span class="big">travel</span>, and I love <span class="big">PHP</span>!.</p>
                <p>I love my work and enjoy each new project as I get it.
                    Feel free to have a look at my portfolio and don't hesitate to contact me if you think I can be of service to you.</p>
                <!--<p>Now, almost 8 years later, I work full time for the awesome team at Xovak Studio. They kindly took me
                    under their wing just over a year ago and I plan to stay there for the foreseeable future.</p>-->
            </div>
        </div>
        <!-- Skills -->
        <div class="row">
            <div class="label">
                <span class="num">Two.</span>
                <span class="name">Skills</span>
            </div>
            <div class="content">
                <p class="tags">
                    <span>PHP</span>
                    <span>Node JS</span>
                    <span>Electron</span>
                    <span>Yii</span>
                    <span>WordPress</span>
                    <span>HTML</span>
                    <span>CSS</span>
                    <span>JavaScript</span>
                    <span>jQuery</span>
                    <span>Embedded JS</span>
                    <span>MySQL</span>
                    <span>Laravel</span>
                    <span>OpenCart</span>
                </p>
            </div>
        </div>
        <!-- Skills -->
        <!-- Recent Work -->
        <div class="row">
            <div class="label">
                <span class="num">Three.</span>
                <span class="name">Recent Work</span>
            </div>
            <div class="content work">
                <ul class="list-unstyled">
                    <li>
                        <i class="fa fa-external-link"></i> <a href="http://exploride.com" target="_blank">exploride.com</a>
                        <span>
                            <i class="fa fa-wordpress"></i>
                            <i class="fa fa-html5"></i>
                        </span>
                    </li>
                    <li>
                        <i class="fa fa-external-link"></i> <a href="http://travelplus.be" target="_blank">travelplus.be</a>
                        <span>
                            <i class="fa fa-wordpress"></i>
                            <i class="fa fa-html5"></i>
                        </span>
                    </li>
                    <li>
                        <i class="fa fa-external-link"></i> <a href="http://rivian.com" target="_blank">rivian.com</a>
                        <span>
                            <i class="fa fa-wordpress"></i>
                            <i class="fa fa-html5"></i>
                        </span>
                    </li>
                    <li>
                        <i class="fa fa-external-link"></i> <a href="http://cheapmooly.com" target="_blank">cheapmooly.com</a>
                        <span>
                            <i class="fa fa-opencart"></i>
                            <i class="fa fa-html5"></i>
                        </span>
                    </li>
                    <li>
                        <i class="fa fa-external-link"></i> <a href="http://www.arkadium.com" target="_blank">arkadium.com</a>
                        <span>
                            <i class="fa fa-wordpress"></i>
                            <i class="fa fa-html5"></i>
                        </span>
                    </li>
                    <li>
                        <i class="fa fa-external-link"></i> <a href="http://www.djwarehouse.com.au" target="_blank">djwarehouse.com.au</a>
                        <span>
                            <i class="fa fa-shopping-cart"></i>
                            <i class="fa fa-html5"></i>
                        </span>
                    </li>
                    <li>
                        <i class="fa fa-external-link"></i> <a href="http://mytool.co.in/" target="_blank">mytool.co.in</a>
                        <span>
                            <i class="fa fa-shopping-cart"></i>
                            <i class="fa fa-html5"></i>
                        </span>
                    </li>
                    <li>
                        <i class="fa fa-external-link"></i> <a href="http://www.gxpeople.eu/" target="_blank">gxpeople.eu</a>
                        <span>
                            <i class="fa fa-wordpress"></i>
                            <i class="fa fa-html5"></i>
                        </span>
                    </li>
                    <li>
                        <i class="fa fa-external-link"></i> <a href="http://www.tesorarestaurant.com/" target="_blank">tesorarestaurant.com</a>
                        <span>
                            <i class="fa fa-wordpress"></i>
                            <i class="fa fa-html5"></i>
                        </span>
                    </li>
                    <li>
                        <i class="fa fa-external-link"></i> <a href="http://modificationpost.com" target="_blank">modificationpost.com</a>
                        <span>
                            <i class="fa fa-wordpress"></i>
                            <i class="fa fa-html5"></i>
                        </span>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Contact -->
        <div class="row">
            <div class="label">
                <span class="num">Four.</span>
                <span class="name">Contact</span>
            </div>
            <div class="content">
                <i class="fa fa-envelope-o"></i> <a href="mailto:png625@gmail.com">png625@gmail.com</a><br/>
                <i class="fa fa-skype"></i> <a href="skype:png625?chat">png625</a><br/>
                <i class="fa fa-phone"></i> 091 9998388980
            </div>
        </div>
    </div>
    <footer>
        <div class="container text-center">
            <p class="social">
                <span><a href="mailto:png625@gmail.com"><i class="fa fa-envelope fa-2x"></i></a></span>
                <span><a target="_blank" href="http://in.linkedin.com/in/imyuvii"><i class="fa fa-linkedin-square fa-2x"></i></a></span>
                <span><a target="_blank" href="http://twitter.com/imyuvii"><i class="fa fa-twitter-square fa-2x"></i></a></span>
                <span><a target="_blank" href="https://github.com/imyuvii"><i class="fa fa-github-square fa-2x"></i></a></span>
            </p>
            <p>&copy; Yuvraj Jhala 2016.</p>
        </div>
    </footer>
</div>
<script type="text/javascript" src="plugins/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="plugins/changethewords/jquery.changethewords.js"></script>
<script type="text/javascript">
    $(function() {
        $("#w").changeWords({
            time: 1500,
            animate: "fadeIn",
            selector: "span"
        });
    });
</script>
</body>
</html>